var http = require('http');
var express = require('express');
var fs = require('fs');


var app = express().use( express.static(__dirname+'/public') );
var server = http.createServer(app).listen(8080);

// server.on( 'request', function( req, res ){
    // console.log( req.url );
// });


var io = require('socket.io').listen( server );

io.sockets.on( 'connection', function(socket){
    socket.emit( 'message', 'connected' );
    socket.on( 'message', function(msg){
        switch( msg )
        {
            case 'look_for_model':
            {
                var modelName = arguments[1];
                var modelCenter = arguments[2];
                fs.readdir( __dirname+'/public/models', function(err,files) {
                    var doesModelExist = (files.indexOf(modelName) >= 0);
                    if( doesModelExist )
                        fs.readFile( __dirname+'/public/models/'+modelName+'/parameters.txt', 'utf8', function(err,content){
                            var paramArr = err?  []  :  content.split( /\s/ ).filter( function(val){ return val.length != 0; } );
                            var params = new Object();
                            for( var p=0; p<paramArr.length; p+=2 )
                                params[paramArr[p]] = paramArr[p+1];
                            socket.emit( 'message', 'model_initialization', modelName, params, modelCenter );
                        });
                });

                break;
            }
            case 'ask_for_loading':
            {
                var modelName = arguments[1];
                fs.readdir( __dirname+'/public/models', function(err,files) {
                    var doesModelExist = (files.indexOf(modelName) >= 0);
                    if( doesModelExist )
                        socket.emit( 'message', 'model_loading', modelName );
                });
                
                break;
            }
        }
    });
});
