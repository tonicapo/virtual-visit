function TileEnumerator()
{
    this.reset();
}

TileEnumerator.prototype.reset = function()
{
    this.lvl = 0;
    this.lvlChanged = false;

    this.y =  0;
    this.x = -1;
    this.count = 1;
    
    this.name = "";
}

TileEnumerator.prototype.next = function()
{
    this.lvlChanged = false;

    if( ++this.x == this.count )
    {
        this.x = 0;
        if( ++this.y == this.count )
        {
            this.y = 0;
            ++ this.lvl
            this.count <<= 1;
            this.lvlChanged = true;
        }
    }

    this.name = "tile_" + (this.lvl+100).toString().slice(1) +
                '_'     + (this.y  +100).toString().slice(1) +
                '_'     + (this.x  +100).toString().slice(1);
}

TileEnumerator.prototype.calibrateCamera = function( cam )
{
    cam.left   = -0.5 - this.x;
    cam.right  = this.count + cam.left;
    cam.bottom = -0.5 - this.y;
    cam.top    = this.count + cam.bottom;
    cam.updateProjectionMatrix();
}
