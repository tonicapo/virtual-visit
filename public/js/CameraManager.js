function CameraManager( camera )
{
    this.camera = camera;
    this.calibrationAngle = 0.0;
    this.recalibrate = true;
}

CameraManager.prototype.applyCalibration = function()
{
    if( this.calibrationAngle == 0.0 )
    {
        // Recovers the head front axis, and checks if it has already been initialized to a correct value.
        if( this.recalibrate )
        {
            this.recalibrate = false;
            return false;
        }

        // Extracts the current head rotation angle in the horizontal plane (around Y axis).
        var horizRotComponent = new THREE.Vector3( this.camera.matrix.elements[2], 0.0, this.camera.matrix.elements[10] );
        this.calibrationAngle = horizRotComponent.angleTo( new THREE.Vector3(0,0,1) );
    }

    var headCalibrationQuat = new THREE.Quaternion();
    headCalibrationQuat.setFromAxisAngle( new THREE.Vector3(0,1,0), this.calibrationAngle );

    var headRotationQuat = new THREE.Quaternion()
    headRotationQuat.setFromEuler( this.camera.rotation );

    this.camera.rotation.setFromQuaternion( headCalibrationQuat.multiply(headRotationQuat) );

    return true;
}

CameraManager.prototype.askForRecalibration = function()
{
    this.calibrationAngle = 0.0;
    this.recalibrate = true;
}
