// Use arrow to control object orientation
/**
/**
 * Created by
 * @author tonicapo
 */

THREE.ItemControls = function(item){
    var self = this;
    // Boolean tag for animation loop
    self.action = false;
    // Create quaternions to handle rotations
    self.manualRotation = new THREE.Quaternion();
    self.arrowRotate = new THREE.Quaternion();
    // Initialize timestamp
    self.updateTime = 0;
    // Initiliaze rotation multiplier
    self.multiplier = 1;
    // Initalize set of controls
    self.manualControls = {
      38 : {index: 0, sign: -1, active: 0},  // up
      40 : {index: 0, sign: 1, active: 0}, // down
      37 : {index: 1, sign: 1, active: 0}, // left
      39 : {index: 1, sign: -1, active: 0},   // right
      191 : {index: 2, sign: 1, active: 0}, // fwd slash
      222 : {index: 2, sign: -1, active: 0}   // single quote
    };

    // Add key eventListeners to the scene
    document.addEventListener('keydown', function(event) { key(event, 1); }, false);
    document.addEventListener('keyup', function(event) { key(event, -1); }, false);

    //At key event, call key function
    function key(event, sign) {
      console.log(event.keyCode);
      if(sign===1 ){
            self.action = true;
            var control = self.manualControls[event.keyCode];
            if (typeof control === 'undefined' || sign === 1 && control.active || sign === -1 && !control.active) {
              return;
            }

            if(control.index === 0){
              self.arrowRotate.setFromAxisAngle( new THREE.Vector3( control.sign * 1, 0, 0 ), Math.PI / 2 );
            }
            else if(control.index===1){
              self.arrowRotate.setFromAxisAngle( new THREE.Vector3( 0, control.sign * 1, 0 ), Math.PI / 2 );
            }else if(control.index===2){
              self.arrowRotate.setFromAxisAngle( new THREE.Vector3( 0, 0, control.sign * 0.1 ), Math.PI / 2 );
            }
        }else{
              console.log('Item rotation OFF');
              self.action=false;
      }
    }

    // Update loop for item animation
    self.update = function(){
      // Time is handled out of action test for smoother rotations at keypress
      var oldTime = self.updateTime;
      var newTime = Date.now();
      self.updateTime = newTime;


          if(self.action){
                var interval = (newTime - oldTime) * 0.001;
                // console.log('update'+interval);
                var update = new THREE.Quaternion(self.arrowRotate.x * interval,
          		                               self.arrowRotate.y * interval,
          		                               self.arrowRotate.z * interval, 1.0);
          		  update.normalize();

                itemQuat = item.quaternion;
                itemQuat.multiplyQuaternions(update, itemQuat);
                itemQuat.normalize();

                item.setRotationFromQuaternion(itemQuat);
          }
      };

};
