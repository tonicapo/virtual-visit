// Use arrow to control object orientation
/**
/**
 * Created by
 * @author tonicapo
 */

THREE.CameraTranslateControls = function(camera){
    var self = this;
    // Boolean tag for animation loop
    self.action = false;
    // Create quaternions to handle rotations
    self.manualRotation = new THREE.Quaternion();
    self.arrowRotate = new THREE.Quaternion();
    self.cameraTranslation = 0;
    self.cameraDirection = "";
    // Initialize timestamp
    self.updateTime = 0;
    // Initiliaze rotation multiplier
    self.multiplier = 1;
    // Initalize set of controls
    self.manualControls = {
      // Azerty camera action controls : a, e, z, q, s, d
  		// Y axis rotation
  		65 : {index: 4, sign: 1, active: 0},  // a
  		69 : {index: 4, sign: -1, active: 0},  // e
  		// Move forward/backward
  		83 : {index: 5, sign: 1, active: 0}, // s
  		90 : {index: 5, sign: -1, active: 0}, // z
  		// Move left/right
  		81 : {index: 6, sign: -1, active: 0}, // q
  		68 : {index: 6, sign: 1, active: 0},  // d
    };

    // Add key eventListeners to the scene
    // document.addEventListener('keydown', function(event) { key(event, 1); }, false);
    // document.addEventListener('keyup', function(event) { key(event, -1); }, false);

    //At key event, call key function
    function key(event, sign) {
      console.log(event.keyCode);
      if(sign===1){
            console.log('Camera translation ON');
            self.action = true;
            var control = self.manualControls[event.keyCode];
            if (typeof control === 'undefined' || sign === 1 && control.active || sign === -1 && !control.active) {
              return;
            }

            if(control.index === 6){
              self.direction = "X";
              self.cameraTranslation = control.sign;
            }else if(control.index === 5){
              self.direction =  "Z";
              self.cameraTranslation = control.sign;
            }else if (control.index === 4){
              return;
            }
      }else{
              console.log('Item rotation OFF');
              self.action=false;
      }
    }

    // Update loop for item animation
    self.update = function(){
      var oldTime = self.updateTime;
      var newTime = Date.now();
      self.updateTime = newTime;

          if(self.action){
                var interval = (newTime - oldTime) * 0.1;
                // console.log('update'+interval);
                // camera.translateX(self.translateX * interval)
                if(self.direction == "X"){
                    camera.position.x += self.cameraTranslation * interval;
                }else if(self.direction == "Z"){
                    camera.position.z += self.cameraTranslation * interval;
                }

          }
      };

};
