// Source :
// https://github.com/mdn/webvr-tests/blob/gh-pages/vrdevice/index.html

if(navigator.getVRDevices){
  navigator.getVRDevices().then(function(myDevices){
    reportDevices(myDevices);
  });
}else{
  console.log("WebVR API not supported by this browser");
}

function reportDevices(devices){
  console.log('Startin report. Devices : '+ devices.length);
  for(i = 0; i < devices.length; i++) {
    //var listItem = document.createElement('li');
    // listeItem.innerHTML =
      console.log('Device'+(i+1) + ': <strong>Hardware ID</strong>: ' + devices[i].hardwareUnitId + ', <strong>VD Device ID</strong>: ' + devices[i].deviceId + ', <strong>VR Device Name</strong>: ' + devices[i].deviceName + '.');
    //list.appendChild(listItem);
    }

    if(devices.length===0){
        console.log('Even if no VR devices are connected, you will likely still get some test results returned for the moment, for example two (Cardboard) devices in the case of Firefox Nightly.');
      }

}
