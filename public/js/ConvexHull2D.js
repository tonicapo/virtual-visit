ConvexHull2D = function( maxPointCount )
{
    this.edgePoints = null;
}

ConvexHull2D.prototype.add = function( x, y )
{
    var onRightSideOf = function( e, x, y )
    {
        var edgeX = e.next.x - e.x;
        var edgeY = e.next.y - e.y;
        var pointX = x - e.x;
        var pointY = y - e.y;
        return edgeX*pointY < edgeY*pointX;
    };

    // Insert the first point.

    if( this.edgePoints == null )
    {
        this.edgePoints = { x:x, y:y, right:false };
        this.edgePoints.prev = this.edgePoints;
        this.edgePoints.next = this.edgePoints;
    }

    // Insert the second point.

    else if( this.edgePoints.next == this.edgePoints )
    {
        if( x != this.edgePoints.x  &&  y != this.edgePoints.y )
            this.edgePoints.next = this.edgePoints.prev = { x:x, y:y, right:false, next:this.edgePoints, prev:this.edgePoints };
    }

    // Insert the n-th point, n >= 3.

    else
    {
        // Check on which side of every edge of the current hull lies the new point.
        var e = this.edgePoints;
        var outsideHull = false;
        do
        {
            e.right = onRightSideOf( e, x, y );
            outsideHull |= e.right;
            e = e.next;
        } while( e != this.edgePoints );

        // If the new point is inside the hull, there is nothing to.
        if( !outsideHull )
            return;

        // Otherwise, the hull must be updated. Hull vertices lying at the boundary between
        // the 'front facing' and 'back facing' areas of the hull are identified.
        var beg, end;
        do
        {
            if( e.right && !e.prev.right )
                beg = e;
            if( !e.right && e.prev.right )
                end = e;
            e = e.next;
        } while( e != this.edgePoints );

        // Two new edges are created to link those vertices to the new point.
        this.edgePoints = { x:x, y:y, right:false, next:end, prev:beg };
        beg.next = end.prev = this.edgePoints;
    }
}

ConvexHull2D.prototype.pointCount = function()
{
    var pointCount = 0;

    var e = this.edgePoints;
    if( e != null )
    {
        do
        {
            ++ pointCount;
            e = e.next;
        } while( e != this.edgePoints );
    }

    return pointCount;
}

ConvexHull2D.prototype.asFloat32Array = function()
{
    var res = new Float32Array( 2*this.pointCount() );

    for( var i=0, e=this.edgePoints; i<res.length; e=e.next )
    {
        res[i++] = e.x;
        res[i++] = e.y;
    }

    return res;
}
