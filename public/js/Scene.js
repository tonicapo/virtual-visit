/***************************************\
| Initialize network connection with    |
| the node.js server.                   |
\***************************************/

var socket = io.connect( 'http://localhost:8080' );

socket.on( 'message', function(msg){
    switch( msg )
    {
        case 'connected':  loadEnvironment(); break;
        case 'model_initialization':
        {
            var modelName   = arguments[1];
            var modelParams = arguments[2];
            var modelCenter = arguments[3];

            models[modelName] = {
                mesh        : null,
                center      : modelCenter,
                minAngle    : THREE.Math.degToRad( parseFloat(modelParams.minAngle) )  ||  0.0,
                maxAngle    : THREE.Math.degToRad( parseFloat(modelParams.maxAngle) )  ||  0.0,
                defaultAngle: THREE.Math.degToRad( parseFloat(modelParams.defaultAngle) )  ||  0.0,
                specular    : new THREE.Color( modelParams.specular  ||  '#000000' ),
                shininess   : parseFloat(modelParams.shininess)  ||  0.0
            };

            console.log( "Model " + modelName + " found on server!" );

            break;
        }
        case 'model_loading':
        {
            var modelName = arguments[1];
            console.log( "Loading of model '" + modelName + "'." );
            loadModelAt( modelName );
            break;
        }
    }
});


/***************************************\
| Setup three.js WebGL renderer.        |
\***************************************/

var renderer = new THREE.WebGLRenderer( { antialias: true, preserveDrawingBuffer: true, stencil: false, depth: true, alpha: true } );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFShadowMap;

document.body.appendChild( renderer.domElement );

//Apply VR stereo rendering to renderer
//var effect = new THREE.VREffect( renderer );
//effect.setSize( window.innerWidth, window.innerHeight );

var renderingNeedsUpdate = true;


/***************************************\
| Setup the default scene.              |
\***************************************/

var scene = new THREE.Scene();
var colliders = new ColliderSet();

var areOccludersLoaded = false;

var loadLightMaps = function()
{
    var onLightMapLoading = function( objFileName, materials )
    {
        var objLoader = new THREE.OBJLoader();
        objLoader.setMaterials( materials );
        objLoader.load( objFileName, function( objects ) {
            console.log( "Loading environment light maps..." );

            var lightMapsFound = "";

            objects.children.forEach( function(lightMapObj) {
                if( lightMapObj instanceof THREE.Mesh )
                {
                    var uv2, lightMap, sceneObj;
                    if( (uv2 = lightMapObj.geometry.getAttribute( "uv" ))  &&
                        (lightMap = lightMapObj.material.map)  &&
                        (sceneObj = scene.children.find(function(o){return o.name==lightMapObj.name;})) )
                    {
                        lightMapsFound += sceneObj.name + ', ';

                        sceneObj.geometry.addAttribute( "uv2", uv2 );
                        sceneObj.geometry.needsUpdate = true;

                        sceneObj.material.lightMap = lightMap;
                        sceneObj.material.lightMap.minFilter = THREE.LinearFilter;
                        sceneObj.material.lightMap.magFilter = THREE.LinearFilter;
                        sceneObj.material.lightMap.generateMipmaps = false;
                        sceneObj.material.needsUpdate = true;
                    }

                    lightMapObj.material.dispose();
                    lightMapObj.geometry.dispose();
                }
            });

            console.log( "  * Light map found for the following model(s): " + lightMapsFound.slice(0,lightMapsFound.length-2) + "." );

            renderingNeedsUpdate = true;
        });
    };

    var environmentDOM = document.getElementById('environment');
    if( !environmentDOM )
        return;

    var lightmapMeshName = environmentDOM.getAttribute('lightmap');
    if( !lightmapMeshName )
        return;

    var lightmapMatName = lightmapMeshName.slice( 0, lightmapMeshName.length-3 ) + "mtl";

    // Load the rendering scene.

    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setBaseUrl( './' );
    mtlLoader.setPath( './' );
    mtlLoader.load(
        lightmapMatName,
        function( materials ){
            onLightMapLoading( lightmapMeshName, materials );
        });
}

var loadEnvironment = function()
{
    var onGeometryLoading = function( objFileName, materials )
    {
        var objLoader = new THREE.OBJLoader();

        var defaultMaterial;
        if( materials )
            objLoader.setMaterials( materials );
        else
            defaultMaterial = new THREE.MeshPhongMaterial( {color:0xFFFFFF, specular: 0x000000, side: THREE.FrontSide, shading: THREE.SmoothShading } );

        objLoader.load( objFileName, function( objects ) {
            console.log( "Loading environment..." );

            var avatarsFound = "";
            var portalsFound = "";

            for( var i=objects.children.length-1; i>=0; --i )
            {
                var obj = objects.children[i];
                if( obj instanceof THREE.Mesh )
                {
                    // Object corresponding to a position where a digitized model must be loaded.
                    if( obj.name.startsWith( "model_" ) )
                    {
                        var modelName = obj.name.slice(6);
                        avatarsFound += modelName + ', ';
                        obj.geometry.computeBoundingBox();
                        socket.emit( 'message', 'look_for_model', modelName, obj.geometry.boundingBox.center() );
                    }
                    // Object corresponding the navigation mesh.
                    else if( obj.name == "nav" )
                    {
                        console.log( "  * Navigation mesh found!" );
                        colliders.add( obj, Collider.FromMeshCopy );
                        areOccludersLoaded = true;
                    }
                    // Object corresponding to a portal.
                    else if( obj.name.startsWith( "portal_" ) )
                    {
                        var areaName = obj.name.slice(7);
                        portalsFound += areaName + ', ';
                        colliders.add( obj, Collider.UseAsPortal, areaName );
                    }
                    // Any object belonging to the environment.
                    else
                    {
                        if( !materials )
                        {
                            obj.material.dispose();
                            obj.material = defaultMaterial;
                        }
                        else if( !obj.material.map )
                            obj.geometry.removeAttribute( 'uv' );

                        obj.receiveShadow = true;
                        scene.add( obj );
                    }
                }
            }

            console.log( "  * Avatars for the following model(s) found: " + avatarsFound.slice(0,avatarsFound.length-2) + "." );
            console.log( "  * Portals to the following area(s) found: " + portalsFound.slice(0,portalsFound.length-2) + "." );

            // If no navigation mesh has been found, use a copy of the scene instead.
            if( !areOccludersLoaded )
            {
                console.log( "  * No navigation mesh found: scene used as occluder." );

                for( var i=scene.children.length-1; i>=0; --i )
                {
                    var obj = scene.children[i];
                    if( obj instanceof THREE.Mesh )
                        colliders.add( obj, Collider.FromMeshCopy );
                }

                areOccludersLoaded = true;
            }

            // Load light maps, if any.
            loadLightMaps();

            renderingNeedsUpdate = true;
        });
    };

    var environmentDOM = document.getElementById('environment');
    if( !environmentDOM )
        return;

    var environmentMeshName = environmentDOM.getAttribute('mesh');
    if( !environmentMeshName )
        return;

    var environmentMatName = environmentMeshName.slice( 0, environmentMeshName.length-3 ) + "mtl";

    // Load the rendering scene.

    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setBaseUrl( './' );
    mtlLoader.setPath( './' );
    mtlLoader.load(
        environmentMatName,
        // MTL file found.
        function( materials ){
            onGeometryLoading( environmentMeshName, materials );
        },
        null,
        // MTL file not found.
        function(){
            onGeometryLoading( environmentMeshName, null );
        });
}


/***************************************\
| Recover model information.            |
\***************************************/

var models = [];

var loadModelAt = function( name )
{
    new THREE.OBJLoader().load(
        'models/'+name+'/geometry.obj',
        function( object ) { object.traverse( function( obj ) { if( obj instanceof THREE.Mesh ) {
            var model = models[name];

            obj.material = new THREE.ShaderMaterial( {
                uniforms: THREE.UniformsUtils.merge([
                    THREE.UniformsLib['common'],
                    THREE.UniformsLib['fog'],
                    THREE.UniformsLib['ambient'],
                    THREE.UniformsLib['lights'],
                    {
                        mapNormals : { type: 't', value: null },
                        mapDiffuse : { type: 't', value: null },
                        mapEnv     : { type: 't', value: null },
                        diffuse    : { type: 'c', value: new THREE.Color( 0x8F8F8F ) },
                        emissive   : { type: 'c', value: new THREE.Color( 0x000000 ) },
                        specular   : { type: 'c', value: model.specular },
                        shininess  : { type: 'f', value: model.shininess }
                    }
                ]),

                vertexShader: [
                    "varying vec3 vViewPosition;",
                    "varying vec2 vUv;",

                    THREE.ShaderChunk[ "common" ],
                    THREE.ShaderChunk[ "uv_pars_vertex" ],
                    THREE.ShaderChunk[ "uv2_pars_vertex" ],
                    THREE.ShaderChunk[ "displacementmap_pars_vertex" ],
                    THREE.ShaderChunk[ "envmap_pars_vertex" ],
                    THREE.ShaderChunk[ "lights_phong_pars_vertex" ],
                    THREE.ShaderChunk[ "color_pars_vertex" ],
                    THREE.ShaderChunk[ "morphtarget_pars_vertex" ],
                    THREE.ShaderChunk[ "skinning_pars_vertex" ],
                    THREE.ShaderChunk[ "shadowmap_pars_vertex" ],
                    THREE.ShaderChunk[ "logdepthbuf_pars_vertex" ],

                    "void main() {",

                        THREE.ShaderChunk[ "uv_vertex" ],
                        THREE.ShaderChunk[ "uv2_vertex" ],
                        THREE.ShaderChunk[ "color_vertex" ],

                        THREE.ShaderChunk[ "beginnormal_vertex" ],
                        THREE.ShaderChunk[ "morphnormal_vertex" ],
                        THREE.ShaderChunk[ "skinbase_vertex" ],
                        THREE.ShaderChunk[ "skinnormal_vertex" ],
                        THREE.ShaderChunk[ "defaultnormal_vertex" ],

                    "   vUv = uv;",

                        THREE.ShaderChunk[ "begin_vertex" ],
                        THREE.ShaderChunk[ "displacementmap_vertex" ],
                        THREE.ShaderChunk[ "morphtarget_vertex" ],
                        THREE.ShaderChunk[ "skinning_vertex" ],
                        THREE.ShaderChunk[ "project_vertex" ],
                        THREE.ShaderChunk[ "logdepthbuf_vertex" ],

                    "	vViewPosition = - mvPosition.xyz;",

                        THREE.ShaderChunk[ "worldpos_vertex" ],
                        THREE.ShaderChunk[ "envmap_vertex" ],
                        THREE.ShaderChunk[ "lights_phong_vertex" ],
                        THREE.ShaderChunk[ "shadowmap_vertex" ],
                    "}"
                ].join('\n'),

                fragmentShader: [
                    "uniform vec3 diffuse;",
                    "uniform vec3 emissive;",
                    "uniform vec3 specular;",
                    "uniform float shininess;",
                    "uniform float opacity;",

                    "varying vec2 vUv;",
                    "uniform sampler2D mapNormals;",
                    "uniform sampler2D mapDiffuse;",
                    "uniform samplerCube mapEnv;",
                    "uniform mat3 normalMatrix;",

                    THREE.ShaderChunk[ "common" ],
                    THREE.ShaderChunk[ "color_pars_fragment" ],
                    THREE.ShaderChunk[ "fog_pars_fragment" ],
                    THREE.ShaderChunk[ "bsdfs" ],
                    THREE.ShaderChunk[ "ambient_pars" ],
                    THREE.ShaderChunk[ "lights_pars" ],
                    THREE.ShaderChunk[ "lights_phong_pars_fragment" ],

                    "void main()",
                    "{",
                        "vec4 diffuseColor = texture2D( mapDiffuse, vUv );",
                        "ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );",

                        THREE.ShaderChunk[ "color_fragment" ],
                        THREE.ShaderChunk[ "specularmap_fragment" ],

                        "vec3 normal = 2.0 * texture2D( mapNormals, vUv ).xyz - vec3(1.0);",
                        "normal = normalize( normalMatrix * normal );",

                        "vec3 envColor = textureCube( mapEnv, normal ).xyz;",
                        "reflectedLight.directDiffuse = envColor * diffuseColor.xyz;",
                        
                        // accumulation
                        THREE.ShaderChunk[ "lights_phong_fragment" ],
                        THREE.ShaderChunk[ "lights_template" ],

                        "vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular;",

                        THREE.ShaderChunk[ "fog_fragment" ],

                    "	gl_FragColor = vec4( outgoingLight, diffuseColor.a );",
                    "}"
                ].join('\n'),

                lights: true
            } );

            // Creates a pre-convolved environment from a cube-map camera.

            model.envCam = new THREE.CubeCamera( 0.1, 1000.0, 1 );
            model.envCam.renderTarget.minFilter = THREE.LinearFilter;
            model.envCam.renderTarget.magFilter = THREE.LinearFilter;
            model.envCam.position.copy( model.center );
            scene.add( model.envCam );
            model.envCam.updateCubeMap( renderer, scene );

            // Setup the model's material.

            var mapNormals = new THREE.TextureLoader().load( 'models/'+name+'/normals/tile_00_00_00.png' );
            mapNormals.flipY = false;
            mapNormals.minFilter = THREE.LinearFilter;
            mapNormals.magFilter = THREE.LinearFilter;
            mapNormals.generateMipmaps = false;

            var mapDiffuse = new THREE.TextureLoader().load( 'models/'+name+'/diffuse/tile_00_00_00.png' );
            mapDiffuse.flipY = false;
            mapDiffuse.minFilter = THREE.LinearFilter;
            mapDiffuse.magFilter = THREE.LinearFilter;
            mapDiffuse.generateMipmaps = false;

            model.initialMapDiffuse = mapDiffuse;
            model.initialMapNormals = mapNormals;

            obj.material.uniforms.mapNormals.value = mapNormals;
            obj.material.uniforms.mapDiffuse.value = mapDiffuse;
            obj.material.uniforms.mapEnv.value     = model.envCam.renderTarget;
            obj.castShadow = true;

            // Positionning of the model.

            obj.rotation.y = model.defaultAngle;
            scene.add( obj );

            // Resize the model so that its height is exactly one.

            obj.geometry.computeBoundingBox();

            var vertices = obj.geometry.getAttribute( 'position' );

            var scale = 1.0 / (obj.geometry.boundingBox.max.y - obj.geometry.boundingBox.min.y);
            var centeringX = 0.5*(obj.geometry.boundingBox.min.x + obj.geometry.boundingBox.max.x);
            var centeringY = 0.5*(obj.geometry.boundingBox.min.y + obj.geometry.boundingBox.max.y);
            var centeringZ = 0.5*(obj.geometry.boundingBox.min.z + obj.geometry.boundingBox.max.z);

            for( var i=0; i<vertices.count*vertices.itemSize; i+=vertices.itemSize )
            {
                vertices.array[i+0] = (vertices.array[i+0]-centeringX) * scale;
                vertices.array[i+1] = (vertices.array[i+1]-centeringY) * scale;
                vertices.array[i+2] = (vertices.array[i+2]-centeringZ) * scale;
            }

            vertices.needsUpdate = true;
            obj.geometry.computeBoundingBox();

            model.mesh = obj;
            obj.position.copy( model.center );

            // Ask for map refinement if asked before the model loading was completed.

            if( model == inspectedModel )
            {
                mapLoader.removeAllMaps();
                mapLoader.addMap( 'models/'+name+'/normals', 'png', 'mapNormals' );
                mapLoader.addMap( 'models/'+name+'/diffuse', 'png', 'mapDiffuse' );
                mapLoader.askForReloading();
            }

            renderingNeedsUpdate = true;
        }})}
    );
}


/***************************************\
| Start the progressive loading of      |
| model maps.                           |
\***************************************/

var mapLoader = new ProgressiveMapLoader(
    renderer,
    function( key, tex ) {
        if( inspectedModel  &&  inspectedModel.mesh )
            inspectedModel.mesh.material.uniforms[key].value = tex;
    },
    function() {
        renderingNeedsUpdate = true;
    }
);


/***************************************\
| .      |
\***************************************/

/*
Request animation frame loop function
*/

var clock = new THREE.Clock( true );
var modelRotationSpeed = 0.0;

var navHeadElevation = 0.0;
var navHeadDirection = 0.0;
var navHeadDirSpeed  = 0.0;
var navHeadHeight    = 1.8;
var navSpeed         = new THREE.Vector3( 0, 0, 0 );
var navAccelStrength = 0.5;

var keyUp    = false;
var keyDown  = false;
var keyLeft  = false;
var keyRight = false;

var camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.01, 100 );
camera.position.set( 0, navHeadHeight, 0 );
scene.add( camera );
var renderOcclusionScene = false;

var collisionDetector = new CollisionDetector( 0.1, navHeadHeight, 0.1 );

// var camLight = new THREE.SpotLight( 0xDFDFFF, 1.5, 10, 0.4*Math.PI, 3 );
var camLight = new THREE.SpotLight( 0xDFDFFF, 1.5, 10, 0.25*Math.PI, 0.4 );
scene.add( camLight );
scene.add( camLight.target );

var useAerialCamera = false;
var aerialCamera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.01, 100 );
aerialCamera.rotation.x = -0.5 * Math.PI;

var inspectedModel = null;

var modelSpotIntensitySpeed = 0.0;
var modelSpotMaxIntensity = 1.5;
var modelSpot = new THREE.SpotLight( 0xFFDF8F, 0, 10, 0.15*Math.PI, 0.02 );
modelSpot.castShadow = true;
modelSpot.shadow.camera.near = 1;
modelSpot.shadow.camera.far = 10;
modelSpot.shadow.camera.fov = THREE.Math.radToDeg( 0.5*0.15*Math.PI );
modelSpot.shadow.mapSize.width = 256;
modelSpot.shadow.mapSize.height = 256;
scene.add( modelSpot );
scene.add( modelSpot.target );


var text2 = document.createElement('div');
text2.style.position = 'absolute';
//text2.style.zIndex = 1;    // if you still don't see the label, try uncommenting this
text2.style.width = 100;
text2.style.height = 100;
text2.style.color = "#ffffff";
text2.style.opacity = 0.75;
text2.style.backgroundColor = "none";
text2.innerHTML = "Some text test";
text2.style.fontSize = 2 + 'em';
text2.style.top = 200 + 'px';
text2.style.left = 200 + 'px';
document.body.appendChild(text2);
console.log( text2.style );


function unloadModel( model )
{
    scene.remove( model.mesh );

    model.mesh.geometry.dispose();
    model.mesh.material.dispose();
    model.mesh = null;

    if( model.initialMapDiffuse )
    {
        model.initialMapDiffuse.dispose();
        delete model.initialMapDiffuse;
    }

    if( model.initialMapNormals )
    {
        model.initialMapNormals.dispose();
        delete model.initialMapNormals;
    }
}


function onEnteringArea( areaName )
{
    var areaArg = areaName.split('_');

    switch( areaArg[0] )
    {
        case 'load':
        {
            for( var m in models )
            {
                if( areaArg.includes(m) )
                {
                    if( !models[m].mesh )
                        socket.emit( 'message', 'ask_for_loading', m );
                }
                else if( models[m].mesh )
                    unloadModel( models[m] );
            }

            break;
        }
        case 'refine':
        {
            var modelName = areaArg[1];
            console.log( "Refinement of model '" + modelName + "' required." );

            inspectedModel = models[modelName];

            if( inspectedModel  &&  inspectedModel.mesh )
            {
                mapLoader.removeAllMaps();
                mapLoader.addMap( 'models/'+modelName+'/normals', 'png', 'mapNormals' );
                mapLoader.addMap( 'models/'+modelName+'/diffuse', 'png', 'mapDiffuse' );
                mapLoader.askForReloading();

                modelSpotIntensitySpeed = 0.5;
                modelSpot.position.set( inspectedModel.center.x, 5.0, inspectedModel.center.z );
                modelSpot.target.position.set( inspectedModel.center.x, 0.0, inspectedModel.center.z );
            }

            break;
        }
        default:
        {
            console.log( "Entering area '" + areaName + "'." );

            for( var m in models )
                if( models[m].mesh )
                    unloadModel( models[m] );

            break;
        }
    }
}

function onLeavingArea( areaName )
{
    var areaArg = areaName.split('_');

    switch( areaArg[0] )
    {
        case 'refine':
        {
            console.log( "Stopping refinement of model '" + areaArg[1] + "'." );

            modelSpotIntensitySpeed = -5;

            if( inspectedModel )
            {
                mapLoader.stop();
                inspectedModel.mesh.material.uniforms.mapNormals.value = inspectedModel.initialMapNormals;
                inspectedModel.mesh.material.uniforms.mapDiffuse.value = inspectedModel.initialMapDiffuse;
                inspectedModel = null;
            }

            break;
        }
    }
}

function animate()
{
    var dt = clock.getDelta();

    // Animate scene.

    for( var i in models )
        if( models[i].mesh )
        {
            if( models[i].minAngle == models[i].maxAngle )
                models[i].mesh.rotation.y += 0.25 * dt;
            else
            {
                var amp = 0.5*(models[i].maxAngle - models[i].minAngle);
                var mid = 0.5*(models[i].maxAngle + models[i].minAngle);
                var s = amp * Math.cos( 0.25 * clock.getElapsedTime() );
                models[i].mesh.rotation.y = mid + s ;
            }
            renderingNeedsUpdate = true;
        }

    if( modelSpotIntensitySpeed != 0 )
    {
        modelSpot.intensity += modelSpotIntensitySpeed * dt;
        if( modelSpot.intensity > modelSpotMaxIntensity )
        {
            modelSpot.intensity = modelSpotMaxIntensity;
            modelSpotIntensitySpeed = 0;
        }
        else if( modelSpot.intensity < 0 )
        {
            modelSpot.intensity = 0;
            modelSpotIntensitySpeed = 0;
        }
    }

    // Update collider set.

    if( areOccludersLoaded )
    {
        colliders.frameUpdate();

        // Set head orientation.

        if( navHeadDirSpeed != 0.0 )
        {
            navHeadDirection += navHeadDirSpeed * dt;
            renderingNeedsUpdate = true;
        }
        camera.rotation.set( navHeadElevation, navHeadDirection, 0, 'YXZ' );

        // Set acceleration according to the triggered controls.

        var cosCamDir = Math.cos( navHeadDirection );
        var sinCamDir = Math.sin( navHeadDirection );

        var headRight = new THREE.Vector3(  cosCamDir, 0, -sinCamDir );
        var headFront = new THREE.Vector3( -sinCamDir, 0, -cosCamDir );

        var acceleration = new THREE.Vector3( 0, 0, 0 );
        var isAccelerationNotNull = false;
        if( keyUp && !keyDown )
        {
            acceleration.add( headFront );
            isAccelerationNotNull = true;
        }
        else if( keyDown && !keyUp )
        {
            acceleration.sub( headFront );
            isAccelerationNotNull = true;
        }
        if( keyLeft && !keyRight )
        {
            acceleration.sub( headRight );
            isAccelerationNotNull = true;
        }
        else if( keyRight && !keyLeft )
        {
            acceleration.add( headRight );
            isAccelerationNotNull = true;
        }

        // Update speed.

        if( isAccelerationNotNull )
        {
            acceleration.normalize().multiplyScalar( navAccelStrength*dt );
            navSpeed.add( acceleration );
        }

        // Apply friction force.

        var isSpeedNull = true;

        for( var i=0; i<3; i+=2 )   // X and Z only (speed along Y axis is always null)
        {
            var speed = navSpeed.getComponent(i);
            var newSpeed = speed - 6*speed*dt;
            if( Math.abs(newSpeed) < 0.0001  ||  speed*newSpeed < 0.0 )
                 navSpeed.setComponent( i, 0.0 );
            else
            {
                navSpeed.setComponent( i, newSpeed );
                isSpeedNull = false;
            }
        }

        // Perform collision test.

        if( !isSpeedNull )
        {
            collisionDetector.checkCollisions( renderer, colliders, camera.position, navSpeed, onEnteringArea, onLeavingArea );
            renderingNeedsUpdate = true;
        }

        // Update position.

        camera.position.add( navSpeed );
    }

    camLight.position.set( camera.position.x, camera.position.y, camera.position.z );
    var headFront = new THREE.Vector3( -sinCamDir, 0, -cosCamDir ).multiplyScalar( 0.5 );
    // camLight.target.position.set( camera.position.x, 0.0, camera.position.z ).add( headFront );
camLight.target.position.set( camera.position.x, camera.position.y, camera.position.z ).add( headFront );

    aerialCamera.position.set( camera.position.x, 15, camera.position.z );

    mapLoader.keepOnLoading();


    if( inspectedModel )
        inspectedModel.envCam.updateCubeMap( renderer, scene );


    //Update VR headset position and apply to camera.
    // controls.update();
    // cameraMng.applyCalibration();

    // Render the scene through the VREffect.
    //effect.render( scene, camera );
    if( renderingNeedsUpdate )
    {
        renderer.render( scene, useAerialCamera? aerialCamera : camera );
        if( renderOcclusionScene )
        {
            renderer.render( colliders.scene, useAerialCamera? aerialCamera : camera );
            renderer.autoClear = false;
            renderer.render( colliders.portals, useAerialCamera? aerialCamera : camera );
            renderer.autoClear = true;
        }
        renderingNeedsUpdate = false;
    }
    requestAnimationFrame( animate );
}

animate();	// Kick off animation loop



/***************** TODO: Generate Your VR Scene Above *****************/



/*
Listen for mouse events.
*/

var mouseSpeed = 6;

document.body.addEventListener( 'mousemove', function(e){
    var x = 0.5 - e.clientX/window.innerWidth;
    if( x < -0.1 )
        navHeadDirSpeed = 3.0 * (x+0.1) * Math.PI;
    else if( x > 0.1 )
        navHeadDirSpeed = 3.0 * (x-0.1) * Math.PI;
    else
        navHeadDirSpeed = 0.0;
    navHeadElevation = 0.6 * (0.5 - e.clientY/window.innerHeight) * Math.PI;
    renderingNeedsUpdate = true;
});

window.addEventListener( "mouseout", function(event) {
    navHeadDirSpeed = 0.0;
}, true);

document.body.addEventListener( 'dblclick', function(e){
    console.log( "double click!" );
});


/*
Listen for keyboard events.
*/

var printKeyCode = false;

window.addEventListener( "keydown", function(event){
    if( printKeyCode )
        console.log( event.keyCode );

    event.preventDefault();
    switch( event.keyCode )
    {
        case 65:    // a
        {
            useAerialCamera = !useAerialCamera;
            renderingNeedsUpdate = true;
            break;
        }
        case 222:   // �
        {
            printKeyCode = !printKeyCode;
            break;
        }
        case 116:   // F5
        {
            window.location.reload();
            break;
        }
        case 32:    // space bar.
        {
            renderOcclusionScene = !renderOcclusionScene;
            if( renderOcclusionScene )
                colliders.scene.add( camera );
            else
                scene.add( camera );
            renderingNeedsUpdate = true;
            break;
        }
        case 76:    // l
        {
            camLight.intensity = camLight.intensity==0?  1.5  :  0;
            renderingNeedsUpdate = true;
            break;
        }
        case 87:    // w
        {
            for( var i=0; i<models.length; ++i )
                if( models[i].mesh )
                {
                    models[i].mesh.material.wireframe = !models[i].mesh.material.wireframe;
                    renderingNeedsUpdate = true;
                }
            break;
        }
        case 37:  keyLeft  = true; break;
        case 38:  keyUp    = true; break;
        case 39:  keyRight = true; break;
        case 40:  keyDown  = true; break;
    }
}, true );

window.addEventListener( "keyup", function(event){
    event.preventDefault();
    switch( event.keyCode )
    {
        case 37:  keyLeft  = false; break;
        case 38:  keyUp    = false; break;
        case 39:  keyRight = false; break;
        case 40:  keyDown  = false; break;
    }
}, true );


/*
Handle window resizes.
*/
window.addEventListener( 'resize', function() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    // effect.setSize( window.innerWidth, window.innerHeight );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderingNeedsUpdate = true;
}, false );
