var CollisionDetector = function( footHeight, headHeight, detectionRadius, resX, resY )
{
    if( typeof(footHeight.valueOf()) != 'number' )
        throw "CollisionDetector error: constructor's 'footHeight' argument must be a number.";
    if( typeof(headHeight.valueOf()) != 'number' )
        throw "CollisionDetector error: constructor's 'headHeight' argument must be a number.";
    if( typeof(detectionRadius.valueOf()) != 'number' )
        throw "CollisionDetector error: constructor's 'detectionRadius' argument must be a number.";

    resX = resX || 16;
    resY = resY || 32;

    this.footHeight = footHeight;
    this.headHeight = headHeight;
    this.detectionRadius = detectionRadius;

    this.detectionCamera = new THREE.OrthographicCamera( -detectionRadius, detectionRadius, headHeight, footHeight, 0, detectionRadius );
    this.detectionRTT    = new THREE.WebGLRenderTarget( resX, resY, {minFilter: THREE.NearestFilter, magFilter: THREE.NearestFilter, generateMipmaps: false, stencilBuffer: false, format: THREE.RGBAFormat, type: THREE.FloatType} );

    this.currentArea = null;
}

CollisionDetector.prototype.checkCollisions = function( renderer, colliderSet, position, speed, onEnteringArea, onLeavingArea )
{
    if( !(speed instanceof THREE.Vector3) )
        throw "CollisionDetector error: 'speed' argument must be a THREE.Vector3!";

    var w = this.detectionRTT.width;
    var h = this.detectionRTT.height;
    var nSamples = 4 * w * h;
    var nIter = 0;

    var gl = renderer.getContext();
    var collisionSamples = new Float32Array( nSamples );

    var extraSpeed = new THREE.Vector3(), colliderNormal = new THREE.Vector3();

    do
    {
        // Set the detection camera at the right position and orientation, so that it looks along speed direction.

        this.detectionCamera.position.x = position.x;
        this.detectionCamera.position.z = position.z;
        this.detectionCamera.rotation.y = Math.atan2( -speed.x, -speed.z );

        // Detection camera clipping planes are modified so as to account for the displacement induced by the current speed vector.

        this.detectionCamera.near = 0.0;
        this.detectionCamera.far  = this.detectionRadius + speed.length();
        this.detectionCamera.updateProjectionMatrix();

        // Render occluders so as to recover those standing in the detection area.

        renderer.render( colliderSet.scene, this.detectionCamera, this.detectionRTT, true );
        gl.readPixels( 0, 0, w, h, gl.RGBA, gl.FLOAT, collisionSamples );

        // Recover the valid sample corresponding to the closest occluder.

        var closestDist = this.detectionCamera.far;
        var sampleId = -1;

        for( var i=nSamples-4; i>0; i-=4 )
        {
            var d = collisionSamples[i+2];
            if( d != 0  &&  d < closestDist )
            {
                closestDist = d;
                sampleId = i;
            }
        }

        // If no occluder has been detected, there is nothing to do.

        if( sampleId < 0 )
            break;

        // Otherwise, speed is modified so as to 'slide' along the occluder's surface, and another detection pass
        // is performed wrt. that modified speed, in order to account for possible corners in occluders' geometry.

        extraSpeed.copy( speed ).normalize().multiplyScalar( Math.abs(closestDist - this.detectionRadius) );
        colliderNormal.set( collisionSamples[sampleId+0], 0.0, collisionSamples[sampleId+1] ).multiplyScalar( colliderNormal.dot(extraSpeed) );
        speed.sub( colliderNormal );
    } while( ++nIter < 8 );

    // Render portals so as to recover those standing in the detection area.

    renderer.render( colliderSet.portals, this.detectionCamera, this.detectionRTT, true );
    gl.readPixels( 0, 0, w, h, gl.RGBA, gl.FLOAT, collisionSamples );

    // Check if a portal has been crossed. In that case, manage the changing of area.

    for( var i=nSamples-4; i>0; i-=4 )
    {
        var portalId = parseInt( collisionSamples[i+2] ) - 1;
        if( portalId >= 0 )
        {
            var reachedArea = colliderSet.portals.children[ portalId ].portalToArea;
            if( reachedArea != this.currentArea )
            {
                if( onLeavingArea !== undefined  &&  this.currentArea )
                    onLeavingArea( this.currentArea );

                this.currentArea = reachedArea;

                if( onEnteringArea !== undefined )
                    onEnteringArea( this.currentArea );
            }
            break;
        }
    }
}

CollisionDetector.prototype.dispose = function()
{
    this.detectionRTT.dispose();
}
