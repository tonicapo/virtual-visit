const EigenSolver = {};

EigenSolver.Symmetric3x3 = function( A, eVal, eVec, aggressive, sortType )
{
    const a00 = A.elements[0], a01 = A.elements[3], a02 = A.elements[6];
    const a11 = A.elements[4], a12 = A.elements[7];
    const a22 = A.elements[8];

    sortType   = (sortType  ===undefined)? 0     : sortType;
    aggressive = (aggressive===undefined)? false : aggressive;

    var Update0 = function( Q, c, s )
    {
        for( var r=0; r<3; ++r )
        {
            var tmp0 = c*Q[r] + s*Q[r+3];
            var tmp1 = Q[r+6];
            var tmp2 = c*Q[r+3] - s*Q[r];
            Q[r]   = tmp0;
            Q[r+3] = tmp1;
            Q[r+6] = tmp2;
        }
    };
    var Update1 = function( Q, c, s )
    {
        for( var r=0; r<3; ++r )
        {
            var tmp0 = c*Q[r+3] - s*Q[r+6];
            var tmp1 = Q[r];
            var tmp2 = c*Q[r+6] + s*Q[r+3];
            Q[r]   = tmp0;
            Q[r+3] = tmp1;
            Q[r+6] = tmp2;
        }
    };
    var Update2 = function( Q, c, s )
    {
        for( var r=0; r<3; ++r )
        {
            var tmp0 = c*Q[r] + s*Q[r+3];
            var tmp1 = s*Q[r] - c*Q[r+3];
            Q[r]   = tmp0;
            Q[r+3] = tmp1;
        }
    };
    var Update3 = function( Q, c, s )
    {
        for( var r=0; r<3; ++r )
        {
            var tmp0 = c*Q[r+3] + s*Q[r+6];
            var tmp1 = s*Q[r+3] - c*Q[r+6];
            Q[r+3] = tmp0;
            Q[r+6] = tmp1;
        }
    };
    var GetCosSin = function( u, v, cs )
    {
        var maxAbsComp = Math.max( Math.abs(u), Math.abs(v) );
        if( maxAbsComp > 0 )
        {
            u /= maxAbsComp;
            v /= maxAbsComp;
            var len = Math.sqrt( u*u + v*v );
            cs.cos = u / len;
            cs.sin = v / len;
            if( cs.cos > 0 )
            {
                cs.cos = -cs.cos;
                cs.sin = -cs.sin;
            }
        }
        else
        {
            cs.cos = -1;
            cs.sin =  0;
        }
    };
    var Converged = function( aggressive, bDiag0, bDiag1, bSuper )
    {
        if( aggressive )
            return (bSuper == 0);
        else
        {
            var sum = Math.abs(bDiag0) + Math.abs(bDiag1);
            return (sum + Math.abs(bSuper) == sum);
        }
    };
    var Sort = function( d, ordered )
    {
        var odd;
        if( d[0] < d[1] )
        {
            if( d[2] < d[0] )       { ordered.i0 = 2; ordered.i1 = 0; ordered.i2 = 1; odd = true;  }
            else if( d[2] < d[1] )  { ordered.i0 = 0; ordered.i1 = 2; ordered.i2 = 1; odd = false; }
            else                    { ordered.i0 = 0; ordered.i1 = 1; ordered.i2 = 2; odd = true;  }
        }
        else
        {
            if( d[2] < d[1] )       { ordered.i0 = 2; ordered.i1 = 1; ordered.i2 = 0; odd = false; }
            else if( d[2] < d[0] )  { ordered.i0 = 1; ordered.i1 = 2; ordered.i2 = 0; odd = true;  }
            else                    { ordered.i0 = 1; ordered.i1 = 0; ordered.i2 = 2; odd = false; }
        }
        return odd;
    };

    // Computes the Householder reflection H and B = H*A*H, where b02 = 0.
    var isRotation = false;
    var cs = { cos: 0, sin: 0 };
    GetCosSin( a12, -a02, cs );
    var c = cs.cos, s = cs.sin;
    var Q = [ c, s, 0, s, -c, 0, 0, 0, 1 ];
    var term0 = c * a00 + s * a01;
    var term1 = c * a01 + s * a11;
    var b00 = c * term0 + s * term1;
    var b01 = s * term0 - c * term1;
    term0 = s * a00 - c * a01;
    term1 = s * a01 - c * a11;
    var b11 = s * term0 - c * term1;
    var b12 = s * a02 - c * a12;
    var b22 = a22;

    // Givens reflections, B' = G^T*B*G, preserve tridiagonal matrices.
    const maxIter = 30;
    var iter = 0;
    var c2 = 0.0, s2 = 0.0;

    if( Math.abs(b12) <= Math.abs(b01) )
    {
        var saveB00, saveB01, saveB11;
        for( iter=0; iter<maxIter; ++iter )
        {
            // Compute the Givens reflection.
            GetCosSin( 0.5*(b00-b11), b01, cs );
            c2 = cs.cos;
            s2 = cs.sin;
            s = Math.sqrt( 0.5*(1-c2) );  // >= 1 / sqrt(2)
            c = 0.5 * s2 / s;

            // Update Q by the Givens reflection.
            Update0( Q, c, s );
            isRotation = !isRotation;

            // Update B = Q^T*B*Q, ensuring that b02 is zero and |b12| has strictly decreased.
            saveB00 = b00;
            saveB01 = b01;
            saveB11 = b11;
            term0 = c * saveB00 + s * saveB01;
            term1 = c * saveB01 + s * saveB11;
            b00 = c * term0 + s * term1;
            b11 = b22;
            term0 = c * saveB01 - s * saveB00;
            term1 = c * saveB11 - s * saveB01;
            b22 = c * term1 - s * term0;
            b01 = s * b12;
            b12 = c * b12;

            if( Converged(aggressive, b00, b11, b01) )
            {
                // Compute the Givens reflection.
                GetCosSin( 0.5*(b00-b11), b01, cs );
                c2 = cs.cos;
                s2 = cs.sin;
                s = Math.sqrt( 0.5*(1-c2) );  // >= 1 / sqrt(2)
                c = 0.5 * s2 / s;

                // Update Q by the Householder reflection.
                Update2( Q, c, s );
                isRotation = !isRotation;

                // Update B = Q^T*B*Q.
                saveB00 = b00;
                saveB01 = b01;
                saveB11 = b11;
                term0 = c * saveB00 + s * saveB01;
                term1 = c * saveB01 + s * saveB11;
                b00 = c * term0 + s * term1;
                term0 = s * saveB00 - c * saveB01;
                term1 = s * saveB01 - c * saveB11;
                b11 = s * term0 - c * term1;
                break;
            }
        }
    }
    else
    {
        var saveB11, saveB12, saveB22;
        for( iter=0; iter<maxIter; ++iter )
        {
            // Compute the Givens reflection.
            GetCosSin( 0.5*(b22-b11), b12, cs );
            c2 = cs.cos;
            s2 = cs.sin;
            s = Math.sqrt( 0.5*(1-c2) );  // >= 1 / sqrt(2)
            c = 0.5 * s2 / s;

             // Update Q by the Givens reflection.
            Update1( Q, c, s );
            isRotation = !isRotation;

            // Update B = Q^T*B*Q, ensuring that b02 is zero and |b12| has strictly decreased.
            saveB11 = b11;
            saveB12 = b12;
            saveB22 = b22;
            term0 = c * saveB22 + s * saveB12;
            term1 = c * saveB12 + s * saveB11;
            b22 = c * term0 + s * term1;
            b11 = b00;
            term0 = c * saveB12 - s * saveB22;
            term1 = c * saveB11 - s * saveB12;
            b00 = c * term1 - s * term0;
            b12 = s * b01;
            b01 = c * b01;

            if( Converged(aggressive, b11, b22, b12) )
            {
                // Compute the Givens reflection.
                GetCosSin( 0.5*(b11-b22), b12, cs );
                c2 = cs.cos;
                s2 = cs.sin;
                s = Math.sqrt( 0.5*(1-c2) );  // >= 1 / sqrt(2)
                c = 0.5 * s2 / s;

                // Update Q by the Givens reflection.
                Update3( Q, c, s );
                isRotation = !isRotation;

                // Update B = Q^T*B*Q, ensuring that b02 is zero and |b12| has strictly decreased.
                saveB11 = b11;
                saveB12 = b12;
                saveB22 = b22;
                term0 = c * saveB11 + s * saveB12;
                term1 = c * saveB12 + s * saveB22;
                b11 = c * term0 + s * term1;
                term0 = s * saveB11 - c * saveB12;
                term1 = s * saveB12 - c * saveB22;
                b22 = s * term0 - c * term1;
                break;
            }
        }
    }

    var diagonal = [ b00, b11, b22 ];
    var ordered = {i0: 0, i1: 1, i2: 2};

    if( sortType != 0 )
    {
        if( !Sort( diagonal, ordered ) )
            isRotation = !isRotation;
        if( sortType < 0 )
        {
            var tmp = i0;
            i0 = i2;
            i2 = tmp;
        }
    }

    eVal[0] = diagonal[ordered.i0];
    eVal[1] = diagonal[ordered.i1];
    eVal[2] = diagonal[ordered.i2];
    eVec[0].set( Q[3*ordered.i0+0], Q[3*ordered.i0+1], Q[3*ordered.i0+2] );
    eVec[1].set( Q[3*ordered.i1+0], Q[3*ordered.i1+1], Q[3*ordered.i1+2] );
    eVec[2].set( Q[3*ordered.i2+0], Q[3*ordered.i2+1], Q[3*ordered.i2+2] );

    // Ensure the columns of Q form a right-handed set.
    if( !isRotation )
        eVec[2].negate()

    return iter;
}

EigenSolver.CovarianceMatrix3 = function( points, covMat, mean )
{
    if( mean === undefined )  mean = new THREE.Vector3();

    var scale = 3.0 / points.length;

    // Compute the mean point of the set of points.

    mean.set(0,0,0);

    for( var i=0; i<points.length; )
    {
        mean.x += points[i++];
        mean.y += points[i++];
        mean.z += points[i++];
    }

    mean.multiplyScalar( scale );

    // Compute the covariance matrix.

    covMat.set(0,0,0, 0,0,0, 0,0,0);

    for( var i=0, v=new THREE.Vector3(); i<points.length; i+=3 )
    {
        v.set( points[i], points[i+1], points[i+2] ).sub( mean );

        covMat.elements[0] += v.x * v.x;
        covMat.elements[1] += v.x * v.y;
        covMat.elements[2] += v.x * v.z;
        covMat.elements[4] += v.y * v.y;
        covMat.elements[5] += v.y * v.z;
        covMat.elements[8] += v.z * v.z;
    }

    covMat.elements[3] = covMat.elements[1];
    covMat.elements[6] = covMat.elements[2];
    covMat.elements[7] = covMat.elements[5];

    covMat.multiplyScalar( scale );
}

EigenSolver.CovarianceMatrix2 = function( points, covMat, mean, plane )
{
    var scale = 3.0 / points.length;

    plane = plane.toLowerCase();
    var i1 = plane.includes('x')? 0 : 1;
    var i2 = plane.includes('z')? 2 : 1;

    // Compute the mean point of the set of points.

    var meanArr = [0,0,0];

    for( var i=0; i<points.length; i+=3 )
    {
        meanArr[i1] += points[i+i1];
        meanArr[i2] += points[i+i2];
    }

    meanArr[i1] *= scale;
    meanArr[i2] *= scale;

    // Compute the covariance matrix.

    covMat.set(0,0,0, 0,0,0, 0,0,0);

    for( var i=0, v=[0,0,0]; i<points.length; i+=3 )
    {
        v[i1] = points[i+i1] - meanArr[i1];
        v[i2] = points[i+i2] - meanArr[i2];

        covMat.elements[0] += v[0] * v[0];
        covMat.elements[1] += v[0] * v[1];
        covMat.elements[2] += v[0] * v[2];
        covMat.elements[4] += v[1] * v[1];
        covMat.elements[5] += v[1] * v[2];
        covMat.elements[8] += v[2] * v[2];
    }

    covMat.elements[3] = covMat.elements[1];
    covMat.elements[6] = covMat.elements[2];
    covMat.elements[7] = covMat.elements[5];

    covMat.multiplyScalar( scale );

    // Return the mean point, if required.

    if( mean !== undefined )
        mean.set( meanArr[0], meanArr[1], meanArr[2] );
}
