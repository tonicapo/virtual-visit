const Collider = {
    CollisionMaterial: new THREE.ShaderMaterial({
        vertexShader: [
            "varying vec3 nor;",
            "varying float dist;",
            "void main()",
            "{",
            "   vec4 eyeSpaceCoordinates = modelViewMatrix * vec4( position, 1.0 );",
            "   dist = -eyeSpaceCoordinates.z;",
            "   gl_Position = projectionMatrix * eyeSpaceCoordinates;",
            "   nor = (modelMatrix * vec4(normal, 1.0)).xyz;",
            "}"].join('\n'),
        fragmentShader: [
            "varying vec3 nor;",
            "varying float dist;",
            "void main()",
            "{",
            "   vec2 N = normalize( vec2(nor.x, nor.z) );",
            "   gl_FragColor = vec4( N, dist, 0.0 );",
            "}"].join('\n'),
        side: THREE.FrontSide
    }),

    PortalMaterial: new THREE.ShaderMaterial({
        uniforms : {
            portalId: { type: 'f', value: 0 }
        },
        vertexShader: [
            "void main()",
            "{",
            "   gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
            "}"].join('\n'),
        fragmentShader: [
            "uniform float portalId;",
            "void main()",
            "{",
            "   gl_FragColor = vec4( 1.0, 1.0, portalId+1.0, 0.0 );",
            "}"].join('\n'),
        side: THREE.FrontSide,
    })
};

// The collider is the provided mesh.
Collider.UseAsCollider = function( mesh )
{
    return mesh.geometry;
}

// The provided mesh is a portal.
Collider.UseAsPortal = function( mesh )
{
    return mesh.geometry;
}

// The collider has exactly the same geometry as the provided mesh.
Collider.FromMeshCopy = function( mesh )
{
    if( mesh.geometry instanceof THREE.BufferGeometry )
    {
        var colliderGeometry = new THREE.BufferGeometry();
        colliderGeometry.addAttribute( 'position', mesh.geometry.getAttribute('position') );
        var indexBuffer = mesh.geometry.getAttribute('index');
        if( indexBuffer != null )
            colliderGeometry.addAttribute( 'index', indexBuffer );
        return colliderGeometry;
    }
    else
        return mesh.geometry.clone();
}

// The collider corresponds to the axis aligned bounding box of the provided mesh.
Collider.FromAABBox = function( mesh )
{
    mesh.geometry.computeBoundingBox();
    var bbox = mesh.geometry.boundingBox;

    var size = bbox.size();
    var geom = new THREE.BufferGeometry().fromGeometry( new THREE.BoxGeometry( size.x, size.y, size.z ) );        

    var center = bbox.center();
    var boxVert = geom.getAttribute('position').array;

    for( var i=0; i<boxVert.length; )
    {
        boxVert[i++] += center.x;
        boxVert[i++] += center.y;
        boxVert[i++] += center.z;
    }

    return geom;
}

// The collider corresponds to the bounding box that best fits the provided mesh.
Collider.FromBestFitBox3D = function( mesh, onGround )
{
    onGround = (onGround===undefined)?  false : onGround;

    var vertices;
    if( mesh.geometry instanceof THREE.BufferGeometry )
        vertices = mesh.geometry.getAttribute('position').array;
    else if( mesh.geometry instanceof THREE.Geometry )
    {
        var srcVertices = mesh.geometry.vertices;
        vertices = new Float32Array( 3*srcVertices.length );
        for( var i=srcVertices.length-1, n=0; i>=0; --i )
        {
            var v = srcVertices[i];
            vertices[n++] = v.x;
            vertices[n++] = v.y;
            vertices[n++] = v.z;
        }
    }
    else
        throw "AddCollider error: geometry type not managed!";

    // Perform a PCA on the set of mesh vertices.

    var covMat = new THREE.Matrix3(),  mean = new THREE.Vector3();
    if( onGround )
        EigenSolver.CovarianceMatrix2( vertices, covMat, mean, 'XZ' );
    else
        EigenSolver.CovarianceMatrix3( vertices, covMat, mean );

    var eVal = [], eVec = [new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3()];
    EigenSolver.Symmetric3x3( covMat, eVal, eVec );

    // Compute the mesh bounding box along the PCA axis.

    var pcaBox = new THREE.Box3();
    pcaBox.makeEmpty();

    for( var i=0, v=new THREE.Vector3(); i<vertices.length; i+=3 )
    {
        v.set( vertices[i+0], vertices[i+1], vertices[i+2] ).sub( mean );
        v.set( v.dot(eVec[0]), v.dot(eVec[1]), v.dot(eVec[2]) );
        pcaBox.expandByPoint( v );
    }

    // Creates the box geometry.

    var size = pcaBox.size();
    var geom = new THREE.BufferGeometry().fromGeometry( new THREE.BoxGeometry( size.x, size.y, size.z ) );

    var center = pcaBox.center();
    var boxVert = geom.getAttribute('position').array;

    for( var i=0, v=new THREE.Vector3(); i<boxVert.length; )
    {
        v.set( boxVert[i+0], boxVert[i+1], boxVert[i+2] ).add( center );
        boxVert[i++] = eVec[0].x*v.x + eVec[1].x*v.y + eVec[2].x*v.z + mean.x;
        boxVert[i++] = eVec[0].y*v.x + eVec[1].y*v.y + eVec[2].y*v.z + mean.y;
        boxVert[i++] = eVec[0].z*v.x + eVec[1].z*v.y + eVec[2].z*v.z + mean.z;
    }

    return geom;
}

// The collider corresponds to the bounding box that best fits the provided mesh, align on the ground plane.
Collider.FromBestFitBox2D = function( mesh )
{
    return Collider.FromBestFitBox3D( mesh, true );
}

// The collider is built from the convex hull of the mesh vertices projected onty the XZ plane, extruded along Y.
Collider.FromConvexHull2D = function( mesh )
{
    var yMin, yMax;
    var hull = new ConvexHull2D();

    if( mesh.geometry instanceof THREE.BufferGeometry )
    {
        var srcVertices = mesh.geometry.getAttribute('position').array;

        yMin = yMax = srcVertices[1];

        for( var i=0; i<srcVertices.length; i+=3 )
        {
            hull.add( srcVertices[i+0], srcVertices[i+2] );
            if( srcVertices[i+1] < yMin )
                yMin = srcVertices[i+1];
            else if( srcVertices[i+1] > yMax )
                yMax = srcVertices[i+1];
        }
    }
    else if( mesh.geometry instanceof THREE.Geometry )
    {
        var srcVertices = mesh.geometry.vertices;

        yMin = yMax = srcVertices[0].y;

        for( var i=0; i<srcVertices.length; ++i )
        {
            hull.add( srcVertices[i].x, srcVertices[i].z );
            if( srcVertices[i].y < yMin )
                yMin = srcVertices[i].y;
            else if( srcVertices[i].y > yMax )
                yMax = srcVertices[i].y;
        }
    }
    else
        throw "AddCollider error: geometry type not managed!";


    var hullVert = hull.asFloat32Array();
    var hullSize = hullVert.length >> 1;

    var meanX = 0, meanY = 0;
    for( var i=0; i<hullVert.length; )
    {
        meanX += hullVert[i++];
        meanY += hullVert[i++];
    }

    meanX /= hullSize;
    meanY /= hullSize;


    var vertices = new Float32Array( 4*3*3*hullSize );

    for( var i=0, iPrev=hullVert.length-2, n=0; i<hullVert.length; iPrev=i, i+=2 )
    {
        // Top faces.
        vertices[n++] = hullVert[iPrev];
        vertices[n++] = yMax;
        vertices[n++] = hullVert[iPrev+1];

        vertices[n++] = meanX;
        vertices[n++] = yMax;
        vertices[n++] = meanY;

        vertices[n++] = hullVert[i];
        vertices[n++] = yMax;
        vertices[n++] = hullVert[i+1];

        // Bottom faces.
        vertices[n++] = meanX;
        vertices[n++] = yMin;
        vertices[n++] = meanY;

        vertices[n++] = hullVert[iPrev];
        vertices[n++] = yMin;
        vertices[n++] = hullVert[iPrev+1];

        vertices[n++] = hullVert[i];
        vertices[n++] = yMin;
        vertices[n++] = hullVert[i+1];

        // Side faces.
        vertices[n++] = hullVert[iPrev];
        vertices[n++] = yMin;
        vertices[n++] = hullVert[iPrev+1];

        vertices[n++] = hullVert[iPrev];
        vertices[n++] = yMax;
        vertices[n++] = hullVert[iPrev+1];

        vertices[n++] = hullVert[i];
        vertices[n++] = yMax;
        vertices[n++] = hullVert[i+1];

        vertices[n++] = hullVert[iPrev];
        vertices[n++] = yMin;
        vertices[n++] = hullVert[iPrev+1];

        vertices[n++] = hullVert[i];
        vertices[n++] = yMax;
        vertices[n++] = hullVert[i+1];

        vertices[n++] = hullVert[i];
        vertices[n++] = yMin;
        vertices[n++] = hullVert[i+1];
    }

    return new THREE.BufferGeometry().addAttribute( 'position', new THREE.BufferAttribute(vertices,3) );
}
