var ColliderSet = function()
{
    this.scene = new THREE.Scene();
    this.portals = new THREE.Scene();
}

ColliderSet.prototype.add = function( mesh, creationFunc, areaName )
{
    if( mesh === undefined  ||  !(mesh instanceof THREE.Mesh) )
        throw "AddCollider error: first argument must be a THREE.Mesh object!";

    creationFunc = creationFunc  ||  Collider.FromAABBox;

    // If the provided mesh geometry is empty, there is nothing to do.

    if( mesh.geometry === undefined  ||  mesh.geometry == null )
        return;

    // Create the collider geometry according to the specified mode.

    var colliderGeometry = creationFunc( mesh );

    // Ensure that normal vectors are defined per face (only for colliders, not for portals).

    if( creationFunc != Collider.UseAsPortal )
        if( colliderGeometry instanceof THREE.BufferGeometry )
        {
            var vertArray = colliderGeometry.getAttribute('position').array;

            var normArray;
            if( colliderGeometry.getAttribute('normal') === undefined )
            {
                normArray = new Float32Array( vertArray.length );
                colliderGeometry.addAttribute( 'normal', new THREE.BufferAttribute(normArray,3) );
            }
            else
                normArray = colliderGeometry.getAttribute('normal').array;

            var e1 = new THREE.Vector3();
            var e2 = new THREE.Vector3();

            for( var i=0; i<vertArray.length; i+=9 )
            {
                e1.set( vertArray[i+3]-vertArray[i+0], vertArray[i+4]-vertArray[i+1], vertArray[i+5]-vertArray[i+2] );
                e2.set( vertArray[i+6]-vertArray[i+0], vertArray[i+7]-vertArray[i+1], vertArray[i+8]-vertArray[i+2] );
                e1.cross( e2 ).normalize();
                normArray[i+0] = normArray[i+3] = normArray[i+6] = e1.x;
                normArray[i+1] = normArray[i+4] = normArray[i+7] = e1.y;
                normArray[i+2] = normArray[i+5] = normArray[i+8] = e1.z;
            }
        }
        else if( colliderGeometry instanceof THREE.Geometry )
        {
            colliderGeometry.computeFaceNormals();
            colliderGeometry.normalsNeedUpdate = true;
        }
        else
            throw "AddCollider error: geometry type not managed!";

    // Create the collider mesh and assign to it the detection material.
    // The collider is declared as a child of the provided mesh, if both are different.

    if( creationFunc == Collider.UseAsPortal )
    {
        if( mesh.material )
            mesh.material.dispose();
        mesh.material = Collider.PortalMaterial.clone();
        mesh.portalToArea = areaName;
        this.portals.add( mesh );
        mesh.material.uniforms.portalId.value = parseFloat( this.portals.children.indexOf( mesh ) );
    }
    else if( creationFunc == Collider.UseAsCollider )
    {
        if( mesh.material )
            mesh.material.dispose();
        mesh.material = Collider.CollisionMaterial;
        this.scene.add( mesh );
    }
    else
    {
        var colliderMesh = new THREE.Mesh( colliderGeometry, Collider.CollisionMaterial );
        colliderMesh.isColliderOf = mesh;
        this.scene.add( colliderMesh );
    }
}

ColliderSet.prototype.frameUpdate = function()
{
    for( var i in this.scene.children )
    {
        var collider = this.scene.children[i];
        if( collider.isColliderOf )
        {
            collider.matrix.identity();
            collider.applyMatrix( collider.isColliderOf.matrixWorld );
        }
    }
}

ColliderSet.prototype.dispose = function()
{
    for( var c in this.scene.children )
        if( this.scene.children[c] instanceof THREE.Mesh )
        {
            this.scene.children[c].geometry.dispose();
            this.scene.children[c].material.dispose();
        }

    for( var c in this.portals.children )
        if( this.portals.children[c] instanceof THREE.Mesh )
        {
            this.portals.children[c].geometry.dispose();
            this.portals.children[c].material.dispose();
        }
}
