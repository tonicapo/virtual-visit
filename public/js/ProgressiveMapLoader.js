function ProgressiveMapLoader( renderer, onMapReallocation, onMapUpdate )
{
    if( !(renderer instanceof THREE.WebGLRenderer) )
        throw "ProgressiveMapLoader constructor error: no THREE.WebGLRenderer provided!";

    this.renderer = renderer;
    this.isLoadingLocked = true;

    this.mapKey = [];
    this.map = [];
    this.mapCoarse = [];
    this.tileFilesPath = [];
    this.tileFilesSuffix = [];

    // Register callback functions.

    if( onMapReallocation !== undefined )
        this.onMapReallocation = onMapReallocation; // Callback for map reallocation.

    if( onMapUpdate !== undefined )
        this.onMapUpdate = onMapUpdate;             // Callback for map update.
}

ProgressiveMapLoader.boundLoader = null;

ProgressiveMapLoader.prototype.addMap = function( path, suffix, mapKey )
{
    if( typeof(path.valueOf()) != 'string' )
        throw "ProgressiveMapLoader.addMap function error: 'path' parameter is missing or is not a string!";
    if( typeof(suffix.valueOf()) != 'string' )
        throw "ProgressiveMapLoader.addMap function error: 'suffix' parameter is missing or is not a string!";

    var i = this.map.length;

    if( !path.endsWith('/') )
        path += '/';
    this.tileFilesPath[i] = path;

    if( !suffix.startsWith('.') )
        suffix = '.' + suffix;
    this.tileFilesSuffix[i] = suffix;

    this.mapKey[i] = (mapKey!==undefined)?  mapKey  :  i;
    this.map[i] = new THREE.WebGLRenderTarget( 1, 1, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, generateMipmaps: false, depthBuffer: false, stencilBuffer: false, format: THREE.RGBFormat } );

    console.log( "Map added at [" + this.mapKey[i] + "]: " + path + "tile_00_00_00" + suffix );
}

ProgressiveMapLoader.prototype.removeAllMaps = function()
{
    for( var m in this.map )
        this.map[m].dispose();

    for( var m in this.mapCoarse )
        this.mapCoarse[m].dispose();

    this.mapKey = [];
    this.map = [];
    this.mapCoarse = [];
    this.tileFilesPath = [];
    this.tileFilesSuffix = [];
}

ProgressiveMapLoader.prototype.getMapByKey = function( mapKey )
{
    return this.map[ this.mapKey.indexOf(mapKey) ];
}

ProgressiveMapLoader.prototype.onTileLoad = function( texTile )
{
    if( !(texTile instanceof THREE.Texture) )
        throw "ProgressiveMapLoader.onTileLoad function error: no THREE.Texture provided!";

    var ldr = ProgressiveMapLoader.boundLoader;
    if( !ldr  ||  ldr.map.length == 0 )
        return;

    var i = ldr.currentMapId;

    texTile.format = THREE.RGBFormat;
    texTile.flipY = false;
    texTile.magFilter = THREE.NearestFilter;
    texTile.minFilter = THREE.NearestFilter;
    texTile.generateMipmaps = false;

    var autoClearBackup = ldr.renderer.autoClear;
    ldr.renderer.autoClear = false;

    if( ldr.tile.lvl == 0 )
    {
        ldr.planeRTT0.material.map = texTile;
        ldr.map[i].setSize( texTile.image.width, texTile.image.height );
        ldr.renderer.render( ldr.sceneRTT0, ldr.cameraRTT0, ldr.map[i], false );
    }
    else
    {
        if( ldr.tile.lvlChanged )
        {
            if( ldr.mapCoarse[i] === undefined )
            {
                ldr.mapCoarse[i] = ldr.map[i];
                ldr.map[i] = new THREE.WebGLRenderTarget( ldr.mapCoarse[i].width<<1, ldr.mapCoarse[i].height<<1, { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, generateMipmaps: false, depthBuffer: false, stencilBuffer: false, format: THREE.RGBFormat } );
            }
            else
            {
                var tmp = ldr.mapCoarse[i];
                ldr.mapCoarse[i] = ldr.map[i];
                ldr.map[i] = tmp;
                ldr.map[i].setSize( ldr.mapCoarse[i].width<<1, ldr.mapCoarse[i].height<<1 );
            }

            if( ldr.onMapReallocation !== undefined )
                ldr.onMapReallocation( ldr.mapKey[i], ldr.map[i] );

            ldr.planeRTT0.material.map = ldr.mapCoarse[i];
            ldr.renderer.render( ldr.sceneRTT0, ldr.cameraRTT0, ldr.map[i], false );

            if( i == 0 )
                console.log( "Map level " + (ldr.tile.lvl-1) + " loaded." );
        }

        ldr.planeRTT.material.uniforms.coarseMap.value = ldr.mapCoarse[i];
        ldr.planeRTT.material.uniforms.detailMap.value = texTile;
        ldr.planeRTT.material.uniforms.coarseMapSize.value = new THREE.Vector2( ldr.mapCoarse[i].width, ldr.mapCoarse[i].height );
        ldr.renderer.render( ldr.sceneRTT, ldr.cameraRTT, ldr.map[i], false );
    }

    ldr.renderer.autoClear = autoClearBackup;

    if( ldr.onMapUpdate !== undefined )
        ldr.onMapUpdate( ldr.mapKey[i], ldr.map[i] );

    ldr.isLoadingLocked = false;
}

ProgressiveMapLoader.prototype.onLoadingTermination = function()
{
    if( !ProgressiveMapLoader.boundLoader )
        throw "ProgressiveMapLoader.onLoadingTermination function error: function shoud not be called directly by the user!";

    var ldr = ProgressiveMapLoader.boundLoader;
    console.log( "Map level " + (ldr.tile.lvl-1) + " loaded. No more level." );

    for( var m in ldr.mapCoarse )
        ldr.mapCoarse[m].dispose();
    ldr.mapCoarse = [];

    delete ldr.cameraRTT0;
    delete ldr.sceneRTT0;
    ldr.planeRTT0.geometry.dispose();
    ldr.planeRTT0.material.dispose();
    delete ldr.planeRTT0;

    delete ldr.cameraRTT;
    delete ldr.sceneRTT;
    ldr.planeRTT.geometry.dispose();
    ldr.planeRTT.material.dispose();
    delete ldr.planeRTT;

    delete ldr.currentMapId;
    delete ldr.tile;

    ProgressiveMapLoader.boundLoader = null;
}

ProgressiveMapLoader.prototype.keepOnLoading = function()
{
    // Ensure that tiles are loaded one after the other.
    if( !this.isLoadingLocked  &&  (ProgressiveMapLoader.boundLoader == null  ||  !ProgressiveMapLoader.boundLoader.isLoadingLocked) )
    {
        ProgressiveMapLoader.boundLoader = this;
        this.isLoadingLocked = true;

        // Load the current tile for all provided maps, and then ask for the next tile.
        this.currentMapId --;
        if( this.currentMapId < 0 )
        {
            this.currentMapId = this.map.length - 1;
            this.tile.next();
            this.tile.calibrateCamera( this.cameraRTT );
        }

        // Check if the maximum loading level (if provided) has been reached.
        if( this.tile.lvlChanged  &&  this.tile.lvl > this.maxLvl )
        {
            this.onLoadingTermination();
            return;
        }

        // Launch the multi-threaded texture update.
        var tileFullName = this.tileFilesPath[this.currentMapId] + this.tile.name + this.tileFilesSuffix[this.currentMapId];
        new THREE.TextureLoader().load( tileFullName, this.onTileLoad, null, this.onLoadingTermination );
    }
}

ProgressiveMapLoader.prototype.stop = function()
{
    if( ProgressiveMapLoader.boundLoader == this )
    {
        this.isLoadingLocked = true;
        this.removeAllMaps();
        this.onLoadingTermination();
    }
}

ProgressiveMapLoader.prototype.dispose = function()
{
    this.removeAllMaps();
    this.onLoadingTermination();
}

ProgressiveMapLoader.prototype.askForReloading = function( maxLvl )
{
    this.maxLvl = parseInt(maxLvl)  ||  Number.MAX_VALUE;

    this.isLoadingLocked = false;
    this.currentMapId = 0;
    this.tile = new TileEnumerator();

    // Initialize objects required for render to texture.

    this.cameraRTT0 = new THREE.OrthographicCamera( -0.5, 0.5, 0.5, -0.5, -1, 1 );
    this.sceneRTT0 = new THREE.Scene();
    this.planeRTT0 = new THREE.Mesh( new THREE.PlaneGeometry( 1.0, 1.0 ), new THREE.MeshBasicMaterial( { color: 0xFFFFFF, depthTest: false, depthWrite: false } ) );
    this.sceneRTT0.add( this.planeRTT0 );

    this.cameraRTT = new THREE.OrthographicCamera( -0.5, 0.5, 0.5, -0.5, -1, 1 );
    this.sceneRTT = new THREE.Scene();
    this.planeRTT = new THREE.Mesh( new THREE.PlaneGeometry( 1.0, 1.0 ), new THREE.ShaderMaterial( {
        uniforms: {
            coarseMap    : { type:  't', value: null },
            detailMap    : { type:  't', value: null },
            coarseMapSize: { type: 'v2', value: null }
        },

        vertexShader: [
            "varying vec2 vUv;",
            "void main()",

            "{",
            "   vUv = uv;",
            "   gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
            "}"
        ].join('\n'),

        fragmentShader: [
            "uniform sampler2D coarseMap;",
            "uniform sampler2D detailMap;",
            "uniform vec2 coarseMapSize;",
            "varying vec2 vUv;",

            "void main()",
            "{",
            "   vec3 average = texture2D( coarseMap, (floor( 0.5*gl_FragCoord.xy ) + vec2(0.5)) / coarseMapSize).xyz;",
            "   vec3 details = texture2D( detailMap , vUv ).xyz;",
            "	gl_FragColor = vec4( average + (2.0*255.0*details - vec3(256.0))/255.0, 1.0 );",
            "}"
        ].join('\n'),

        depthTest: false, depthWrite: false
    } ) );
    this.sceneRTT.add( this.planeRTT );
}
