/***************************************\
| Setup three.js WebGL renderer.        |
\***************************************/

var renderer = new THREE.WebGLRenderer( { antialias: true, preserveDrawingBuffer: true, stencil: false, depth: true, alpha: true } );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.shadowMapEnabled = true;
renderer.shadowMapType = THREE.PCFShadowMap;

document.body.appendChild( renderer.domElement );

//Apply VR stereo rendering to renderer
//var effect = new THREE.VREffect( renderer );
//effect.setSize( window.innerWidth, window.innerHeight );

var renderingNeedsUpdate = true;


/***************************************\
| Setup the default scene.              |
\***************************************/

var scene = new THREE.Scene();

var light1 = new THREE.SpotLight( 0xffffff, 1, 20, 1 );
light1.position.set( 2, 5, 2 );
light1.target.position.set( 0, 0.5, 0 );
light1.castShadow = true;
light1.shadowCameraNear = 1;
light1.shadowCameraFar = 20;
light1.shadowCameraFov = 20;
light1.shadowBias = 0.01;
light1.shadowDarkness = 0.5;
light1.shadowMapWidth = 1024;
light1.shadowMapHeight = 1024;
scene.add( light1 );

var light2 = new THREE.SpotLight( 0xffffff, 1, 30, 1 );
light2.position.set( -3, 2, 2 );
light2.target.position.set( 0, 0.5, 0 );
light2.castShadow = true;
light2.shadowCameraNear = 1;
light2.shadowCameraFar = 30;
light2.shadowCameraFov = 30;
light2.shadowBias = 0.01;
light2.shadowDarkness = 0.5;
light2.shadowMapWidth = 1024;
light2.shadowMapHeight = 1024;
scene.add( light2 );

var ground = new THREE.Mesh( new THREE.PlaneGeometry(500,500,1,1), new THREE.MeshPhongMaterial( {color: 0x8F8F8F, specular: 0x000000, shininess: 0 } ) );
ground.rotation.x = THREE.Math.degToRad( -90 );
ground.position.y = 0;
ground.receiveShadow = true;
scene.add( ground );


/***************************************\
| Recover model information.            |
\***************************************/

var currentModel = 0;
var models = [];

var LoadModelAt = function( modelId )
{
    var n = modelId;
    new THREE.OBJLoader().load(
        models[n].name+'/geometry.obj',
        function( object ) { object.traverse( function( obj ) { if( obj instanceof THREE.Mesh ) {
            console.log( n );

            obj.material = new THREE.ShaderMaterial( {
                uniforms: THREE.UniformsUtils.merge([
                    THREE.UniformsLib['common'],
                    THREE.UniformsLib['fog'],
                    THREE.UniformsLib['ambient'],
                    THREE.UniformsLib['lights'],
                    {
                        mapNormals : { type: 't', value: null },
                        mapDiffuse : { type: 't', value: null },
                        diffuse    : { type: 'c', value: new THREE.Color( 0x8F8F8F ) },
                        emissive   : { type: 'c', value: new THREE.Color( 0x000000 ) },
                        specular   : { type: 'c', value: models[n].specular },
                        shininess  : { type: 'f', value: models[n].shininess }
                    }
                ]),

                vertexShader: [
                    "varying vec3 vViewPosition;",
                    "varying vec2 vUv;",

                    THREE.ShaderChunk[ "common" ],
                    THREE.ShaderChunk[ "uv_pars_vertex" ],
                    THREE.ShaderChunk[ "uv2_pars_vertex" ],
                    THREE.ShaderChunk[ "displacementmap_pars_vertex" ],
                    THREE.ShaderChunk[ "envmap_pars_vertex" ],
                    THREE.ShaderChunk[ "lights_phong_pars_vertex" ],
                    THREE.ShaderChunk[ "color_pars_vertex" ],
                    THREE.ShaderChunk[ "morphtarget_pars_vertex" ],
                    THREE.ShaderChunk[ "skinning_pars_vertex" ],
                    THREE.ShaderChunk[ "shadowmap_pars_vertex" ],
                    THREE.ShaderChunk[ "logdepthbuf_pars_vertex" ],

                    "void main() {",

                        THREE.ShaderChunk[ "uv_vertex" ],
                        THREE.ShaderChunk[ "uv2_vertex" ],
                        THREE.ShaderChunk[ "color_vertex" ],

                        THREE.ShaderChunk[ "beginnormal_vertex" ],
                        THREE.ShaderChunk[ "morphnormal_vertex" ],
                        THREE.ShaderChunk[ "skinbase_vertex" ],
                        THREE.ShaderChunk[ "skinnormal_vertex" ],
                        THREE.ShaderChunk[ "defaultnormal_vertex" ],

                    "   vUv = uv;",

                        THREE.ShaderChunk[ "begin_vertex" ],
                        THREE.ShaderChunk[ "displacementmap_vertex" ],
                        THREE.ShaderChunk[ "morphtarget_vertex" ],
                        THREE.ShaderChunk[ "skinning_vertex" ],
                        THREE.ShaderChunk[ "project_vertex" ],
                        THREE.ShaderChunk[ "logdepthbuf_vertex" ],

                    "	vViewPosition = - mvPosition.xyz;",

                        THREE.ShaderChunk[ "worldpos_vertex" ],
                        THREE.ShaderChunk[ "envmap_vertex" ],
                        THREE.ShaderChunk[ "lights_phong_vertex" ],
                        THREE.ShaderChunk[ "shadowmap_vertex" ],
                    "}"
                ].join('\n'),

                fragmentShader: [
                    "uniform vec3 diffuse;",
                    "uniform vec3 emissive;",
                    "uniform vec3 specular;",
                    "uniform float shininess;",
                    "uniform float opacity;",

                    "varying vec2 vUv;",
                    "uniform sampler2D mapNormals;",
                    "uniform sampler2D mapDiffuse;",
                    "uniform mat3 normalMatrix;",

                    THREE.ShaderChunk[ "common" ],
                    THREE.ShaderChunk[ "color_pars_fragment" ],
                    THREE.ShaderChunk[ "fog_pars_fragment" ],
                    THREE.ShaderChunk[ "bsdfs" ],
                    THREE.ShaderChunk[ "ambient_pars" ],
                    THREE.ShaderChunk[ "lights_pars" ],
                    THREE.ShaderChunk[ "lights_phong_pars_fragment" ],

                    "void main()",
                    "{",
                    "   vec4 diffuseColor = texture2D( mapDiffuse, vUv );",
                    "	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );",

                        THREE.ShaderChunk[ "color_fragment" ],
                        THREE.ShaderChunk[ "specularmap_fragment" ],
//                                THREE.ShaderChunk[ "normal_fragment" ],
                        "vec3 normal = 2.0 * texture2D( mapNormals, vUv ).xyz - vec3(1.0);",
                        "normal = normalize( normalMatrix * normal );",

                        // accumulation
                        THREE.ShaderChunk[ "lights_phong_fragment" ],
                        THREE.ShaderChunk[ "lights_template" ],

                        "vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular;",

                        THREE.ShaderChunk[ "fog_fragment" ],

                    "	gl_FragColor = vec4( outgoingLight, diffuseColor.a );",
                    "}"
                ].join('\n'),

                lights: true
            } );

            models[n].mapNormals = new THREE.TextureLoader().load( models[n].name+'/normals/tile_00_00_00.png' ),
            models[n].mapNormals.flipY = false;
            models[n].mapNormals.minFilter = THREE.LinearFilter;
            models[n].mapNormals.magFilter = THREE.LinearFilter;
            models[n].mapNormals.generateMipmaps = false;

            models[n].mapDiffuse = new THREE.TextureLoader().load( models[n].name+'/diffuse/tile_00_00_00.png' ),
            models[n].mapDiffuse.flipY = false;
            models[n].mapDiffuse.minFilter = THREE.LinearFilter;
            models[n].mapDiffuse.magFilter = THREE.LinearFilter;
            models[n].mapDiffuse.generateMipmaps = false;

            obj.material.uniforms.mapNormals.value = models[n].mapNormals;
            obj.material.uniforms.mapDiffuse.value = models[n].mapDiffuse;
            obj.castShadow = true;

            // Positionning of the model.

            obj.rotation.y = models[n].defaultAngle;
            obj.visible = (n == currentModel);
            scene.add( obj );

            // Resize the model so that its height is exactly one.

            obj.geometry.computeBoundingBox();

            var vertices = obj.geometry.getAttribute( 'position' );

            var scale = 1.0 / (obj.geometry.boundingBox.max.y - obj.geometry.boundingBox.min.y);
            var centeringX = 0.5*(obj.geometry.boundingBox.min.x + obj.geometry.boundingBox.max.x);
            var centeringY = obj.geometry.boundingBox.min.y;
            var centeringZ = 0.5*(obj.geometry.boundingBox.min.z + obj.geometry.boundingBox.max.z);

            for( var i=0; i<vertices.count*vertices.itemSize; i+=vertices.itemSize )
            {
                vertices.array[i+0] = (vertices.array[i+0]-centeringX) * scale;
                vertices.array[i+1] = (vertices.array[i+1]-centeringY) * scale;
                vertices.array[i+2] = (vertices.array[i+2]-centeringZ) * scale;
            }

            vertices.needsUpdate = true;
            obj.geometry.computeBoundingBox();

            models[n].mesh = obj;
            renderingNeedsUpdate = true;
        }})}
    );
}


var socket = io.connect( 'http://localhost:8080' );

socket.on( 'message', function(msg){
    switch( msg )
    {
        case 'connected': socket.emit( 'message', 'request_models' ); break;
        case 'model':
        {
            var modelId = parseInt( arguments[1] );
            var modelName = arguments[2];

            models[modelId] = {
                name        : 'models/'+modelName,
                mesh        : null,
                minAngle    : null,
                maxAngle    : null,
                defaultAngle: 0.0,
                specular    : new THREE.Color('#000000'),
                shininess   : 0.0
            };

            socket.emit( 'message', 'require_model_params', modelId, modelName );

            break;
        }
        case 'model_params':
        {
            var modelId = parseInt( arguments[1] );
            var params = arguments[2];

            for( var p=0; p<params.length; p+=2 )
                switch( params[p] )
                {
                    case 'minAngle'    : models[modelId].minAngle     = THREE.Math.degToRad( parseFloat(params[p+1]) ); break;
                    case 'maxAngle'    : models[modelId].maxAngle     = THREE.Math.degToRad( parseFloat(params[p+1]) ); break;
                    case 'defaultAngle': models[modelId].defaultAngle = THREE.Math.degToRad( parseFloat(params[p+1]) ); break;
                    case 'specular'    : models[modelId].specular     = new THREE.Color( params[p+1] ); break;
                    case 'shininess'   : models[modelId].shininess    = parseFloat(params[p+1]); break;
                }

            LoadModelAt( modelId );

            break;
        }
    }
});



/***************************************\
| Start the progressive loading of      |
| model maps.                           |
\***************************************/

var mapLoader = new ProgressiveMapLoader(
    renderer,
    function( key, tex ) {
        if( models[currentModel].mesh )
            models[currentModel].mesh.material.uniforms[key].value = tex;
    },
    function() {
        renderingNeedsUpdate = true;
    }
);


/***************************************\
| .      |
\***************************************/


/*
Request animation frame loop function
*/

var clock = new THREE.Clock( true );
// var cameraMng = new CameraManager( camera );
var modelRotationSpeed   = 0.0;

var cameraElevationAngle = 0.0;
var cameraRotationSpeed  = 0.0;
var cameraDistanceSpeed  = 0.0;
var cameraDistance       = 1.2;
var cameraHeight         = 0.5;
var groundLvlReached     = false;

function recomputeCameraPosition()
{
    cameraDistance = Math.min( Math.max( cameraDistance, 0.3 ), 2.5 );

    var angularLowerLimit = -Math.atan( (cameraHeight-0.05) / cameraDistance );
    if( groundLvlReached )
    {
        cameraElevationAngle = angularLowerLimit;
    }
    else
    {
        cameraElevationAngle = Math.min( Math.max( cameraElevationAngle, angularLowerLimit ), THREE.Math.degToRad(80) );
        groundLvlReached = (cameraElevationAngle == angularLowerLimit);
    }

    camera.position.x = 0.0;
    camera.position.y = cameraDistance * Math.sin( cameraElevationAngle ) + cameraHeight;
    camera.position.z = cameraDistance * Math.cos( cameraElevationAngle );

    camera.lookAt( new THREE.Vector3(scene.position.x, cameraHeight, scene.position.z) );
}

var camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 100 );
recomputeCameraPosition();
scene.add(camera);

//Apply VR headset positional data to camera.
//var controls = new THREE.VRControls( camera );


function updateSpeed( speed, atten, dt )
{
    var newSpeed = speed - atten*speed*dt;
    return ( Math.abs(newSpeed) < 0.0001  ||  speed*newSpeed < 0.0 )?  0.0  :  newSpeed;
}


function animate()
{
    deltaTime = clock.getDelta();

    if( models[currentModel]  &&  models[currentModel].mesh  &&  modelRotationSpeed != 0.0 )
    {
        models[currentModel].mesh.rotation.y += modelRotationSpeed * deltaTime;
        if( models[currentModel].minAngle != null  &&  models[currentModel].mesh.rotation.y < models[currentModel].minAngle )
            models[currentModel].mesh.rotation.y = models[currentModel].minAngle;
        if( models[currentModel].maxAngle != null  &&  models[currentModel].mesh.rotation.y > models[currentModel].maxAngle )
            models[currentModel].mesh.rotation.y = models[currentModel].maxAngle;

        modelRotationSpeed = updateSpeed( modelRotationSpeed, 6, deltaTime );
        renderingNeedsUpdate = true;
    }
    if( cameraRotationSpeed != 0.0 )
    {
        cameraElevationAngle += cameraRotationSpeed * deltaTime;
        cameraRotationSpeed = updateSpeed( cameraRotationSpeed, 6, deltaTime );
        recomputeCameraPosition();
        renderingNeedsUpdate = true;
    }
    if( cameraDistanceSpeed != 0.0 )
    {
        cameraDistance += cameraDistanceSpeed * deltaTime;
        cameraDistanceSpeed = updateSpeed( cameraDistanceSpeed, 6, deltaTime );
        recomputeCameraPosition();
        renderingNeedsUpdate = true;
    }

    mapLoader.keepOnLoading();

    //Update VR headset position and apply to camera.
    // controls.update();
    // cameraMng.applyCalibration();

    // Render the scene through the VREffect.
    //effect.render( scene, camera );
    if( renderingNeedsUpdate )
    {
        renderer.render( scene, camera );
        renderingNeedsUpdate = false;

        var gl = renderer.getContext();
        var collisionDetectionData = new Uint8Array( 4*4*4 );
        gl.readPixels( 0, 0, 4, 4, gl.RGBA, gl.UNSIGNED_BYTE, collisionDetectionData );
    }
    requestAnimationFrame( animate );
}

animate();	// Kick off animation loop



/***************** TODO: Generate Your VR Scene Above *****************/



/*
Listen for click event to enter full-screen mode.
We listen for single click because that works best for mobile for now
*/

var mousePreviousPos;
var rotationMode;

document.body.addEventListener( 'mousedown', function(e){
    mousePreviousPos = new THREE.Vector2( e.clientX, e.clientY );
    rotationMode = 0;
});

document.body.addEventListener( 'mousemove', function(e){
    if( e.buttons & 1 ) // Left button.
    {
        var mouseCurrentPos = new THREE.Vector2( e.clientX, e.clientY );
        var deltaX = mouseCurrentPos.x - mousePreviousPos.x;
        var deltaY = mouseCurrentPos.y - mousePreviousPos.y;
        mousePreviousPos = mouseCurrentPos;

        if( rotationMode == 0  &&  deltaX != 0  &&  deltaY != 0 )
            rotationMode = Math.abs(deltaX)>Math.abs(deltaY)? 1 : 2;

        if( rotationMode == 1 )
            modelRotationSpeed = 250 * deltaX / window.innerWidth;
        else
        {
            cameraRotationSpeed = (250 * deltaY / window.innerWidth);// * (window.innerHeight / window.innerWidth);
            groundLvlReached &= (cameraRotationSpeed < 0.0 );
        }
    }
});

document.body.addEventListener( 'wheel', function(e){
    var dir = e.deltaY / Math.abs(e.deltaY);
    cameraDistanceSpeed = 0.8 * dir * cameraDistance;
});


/*
Listen for keyboard events
*/

var mapLoaderCurrentLvl = 0;

function onkey(event)
{
    event.preventDefault();

    switch( event.keyCode )
    {
        case 37:    // left arrow
        {
            if( currentModel > 0 )
            {
                models[currentModel].mesh.material.uniforms.mapNormals.value = null;
                models[currentModel].mesh.material.uniforms.mapDiffuse.value = null
                models[currentModel--].mesh.visible = false;

                models[currentModel].mesh.visible = true;
                mapLoader.removeAllMaps();
                mapLoader.addMap( models[currentModel].name+'/normals', 'png', 'mapNormals' );
                mapLoader.addMap( models[currentModel].name+'/diffuse', 'png', 'mapDiffuse' );
                mapLoader.askForReloading( mapLoaderCurrentLvl = 4 );
            }
            break;
        }
        case 39:    // right arrow
        {
            if( currentModel < models.length-1 )
            {
                models[currentModel].mesh.material.uniforms.mapNormals.value = null;
                models[currentModel].mesh.material.uniforms.mapDiffuse.value = null
                models[currentModel++].mesh.visible = false;

                models[currentModel].mesh.visible = true;
                mapLoader.removeAllMaps();
                mapLoader.addMap( models[currentModel].name+'/normals', 'png', 'mapNormals' );
                mapLoader.addMap( models[currentModel].name+'/diffuse', 'png', 'mapDiffuse' );
                mapLoader.askForReloading( mapLoaderCurrentLvl = 4 );
            }
            break;
        }
        case 87:    // w
        {
            for( var i=0; i<models.length; ++i )
                if( models[i].mesh )
                {
                    models[i].mesh.material.wireframe = !models[i].mesh.material.wireframe;
                    renderingNeedsUpdate = true;
                }
            break;
        }
        case 82:    // r
        {
            mapLoader.removeAllMaps();
            mapLoader.addMap( models[currentModel].name+'/normals', 'png', 'mapNormals' );
            mapLoader.addMap( models[currentModel].name+'/diffuse', 'png', 'mapDiffuse' );
            mapLoader.askForReloading( mapLoaderCurrentLvl = 3 - mapLoaderCurrentLvl );
            break;
        }
    }
};
window.addEventListener("keydown", onkey, true);

/*
Handle window resizes
*/
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    // effect.setSize( window.innerWidth, window.innerHeight );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderingNeedsUpdate = true;
}
window.addEventListener( 'resize', onWindowResize, false );
