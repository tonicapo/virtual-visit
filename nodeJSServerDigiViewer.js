var http = require('http');
var express = require('express');
var fs = require('fs');


var app = express().use( express.static(__dirname+'/public') );
var server = http.createServer(app).listen(8080);

// server.on( 'request', function( req, res ){
    // console.log( req.url );
// });


var io = require('socket.io').listen( server );

io.sockets.on( 'connection', function(socket){
    socket.emit( 'message', 'connected' );
    socket.on( 'message', function(msg){
        switch( msg )
        {
            case 'request_models':
            {
                fs.readdir( __dirname+'/public/models', function(err,files) {
                    for( var i in files )
                        socket.emit( 'message', 'model', i, files[i] );
                });
                break;
            }
            case 'require_model_params':
            {
                var modelId = parseInt( arguments[1] );
                var modelName = arguments[2];

                fs.readFile( __dirname+'/public/models/'+modelName+'/parameters.txt', 'utf8', function(err,content){
                    var params = err?  []  :  content.split( /\s/ ).filter( function(val){ return val.length != 0; } );
                    socket.emit( 'message', 'model_params', modelId, params );
                });

                break;
            }
            case 'does_model_exists':
            {
                var searchedModel = arguments[2];
                fs.readdir( __dirname+'/public/models', function(err,files) {
                    socket.emit( 'message', 'model_exist', searchedModel, files.indexOf(searchedModel) >= 0 );
                });
                break;
            }
        }
    });
});
